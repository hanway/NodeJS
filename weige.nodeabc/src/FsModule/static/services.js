const http = require('http');
const fs = require('fs');
const path = require('path');
const utils = require('./utils');
http.createServer((req, res) => {
    let pathname = req.url;
    if(pathname === '/'){
        pathname = '/index.html';
    }
    let extname = path.extname(pathname);
    if(pathname !== '/favicon.ico'){
        console.log(pathname);
        res.writeHead(200,{'Content-Type':`${utils.getMime(extname)};charset=utf8`});
        fs.readFile('.'+pathname, (err, data) => {
            if(err){
                console.log('404');
                res.end();
            }else{
                res.write(data);
                res.end();
            }
        })
    }
}).listen(3000);