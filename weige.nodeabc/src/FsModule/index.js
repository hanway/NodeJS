/*
* 1. fs.stat  检测是文件还是目录
* 2. fs.mkdir 创建目录
* 3. fs.writeFile 创建写入文件
* 4. fs.appendFile 追加文件
* 5. fs.readFile 读取文件
* 6. fs.rename 重命名文件
* 7. fs.readdir 读取目录
* 8. fs.rmdir 删除目录
* 9. fs.unlink 删除文件
* */

const fs = require('fs');
/*
* 1. fs.stat 检测是文件还是目录
* */
/*fs.stat('./html', ((err, stats) => {
    if(err) throw err;
    console.log(stats);
    console.log(stats.isDirectory());
    console.log(stats.isFile());
}));*/

/*
* 2. mkdir 创建文件目录
* */
/*
if(fs.existsSync('./css')){
    console.log('文件已存在');
    return
}else{
    fs.mkdir('./css', {recursive: true}, err => {
        if(err) throw err;
    })
}*/

/*
* 3. 写入文件
* */
/*
fs.writeFile('./html/index.html', 'Hello World!', 'utf8', (err)=>{
    if(err) throw err;
    console.log('写入成功')
});*/
/*
* 4. fs.appendFile
* 想文件中追加内容
* 当文件不存在是会自动创建
* */
/*
fs.appendFile('./html/index.html', ' Hello SinoChem!', (err)=>{
    if(err) throw err;
    console.log('创建或追加内容成功')
});*/

/*
*
* 5. fs.readFile
* 读取文件,读取原始的一个buffer数据
* */
/*fs.readFile('./html/index.html', (err, data) => {
    if(err) throw err;
    console.log(data.toString('utf8'));
});*/

/*
* 6. fs.readdir
* 读取文件目录
* */
/*
fs.readdir('./', (err, files) => {
    if(err) throw err;
    console.log(files);
});*/

/*
* 文件重命名 功能：1 表示重命名 2 移动文件
* 7. fs.rename
* */
/*
fs.rename('./html/css.html', './html/css.css', err => {
    if(err) throw err;
    console.log('success!');
});*/

/*
* 8. fs.rmdir
* 删除目录
* */
/*if(fs.existsSync('./css')){
    // 非递归删除
    fs.rmdir('./css', err => {
        if(err) throw err;
        console.log('文件删除成功！')
    })
}

if(fs.existsSync('./css')){
    // 递归删除
    fs.rmdir('./css', { recursive:true },err => {
        if(err) throw err;
        console.log('文件删除成功！')
    })
}*/


/*
* 9. fs.unlink
* 删除文件，使用方法同上
* */

/*
* 文件上传
*
* */
/*const path = './upload';
fs.stat(path, (err, stats) => {
    if(err){
        // 执行创建目录
        mkdir(path);
    }else{
        if(stats.isDirectory()){
            console.log('目录已存在')
        }else{
            // 先删除在执行创建
            fs.unlink(path, (err)=>{
                if(err) throw err;
                mkdir(path);
            });
            mkdir(path)
        }
    }
});
function mkdir(path){
    fs.mkdir(path, err => {
        if(err) throw err;
    })
}*/

/*
* 创建目录第三方模块 mkdirp
* https://www.npmjs.com/package/mkdirp
* */
let arrDir = [];
fs.readdir('./', {withFileTypes:true},(err, files) => {
    if(err) throw err;
    for(let i=0;i<files.length;i++){
        if(files[i].isDirectory()){
            arrDir.push(files[i].name)
        }
    }
    console.log(arrDir);
});