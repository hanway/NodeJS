const fs = require('fs');
/*
* 以流的方式进行文件读取
* 大文件块时读取文件使用
* */
/*let readStream = fs.createReadStream('./story.txt');
let count = 0;
let str = "";
readStream.on('data', (data)=>{
    str += data;
    count++
});
readStream.on('end', ()=>{
    console.log(str);
    console.log(count);
});

readStream.on('error', (err)=>{
    throw err;
});*/

/*
* 以流的方式进行文件写入
* */
/*let str = '';
for(let i=0; i<500; i++){
    str += '我是从数据库中读取的文件，我要保存起来1111\n';
}
let writeStream = fs.createWriteStream('./output.txt');
writeStream.write(str);
/!*
* 标记文件末尾，触发和面的finish事件
* *!/
writeStream.end();
writeStream.on('finish', ()=>{
    console.log('finish');
});*/

/*
* 以管道的形式进行文件的复制
* */
/*
let readStream = fs.createReadStream('./story.txt');
let writeStream = fs.createWriteStream('./html/story.txt');
readStream.pipe(writeStream);*/
