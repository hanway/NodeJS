const http = require('http');
const url = require('url');
let G = {};

const app = function (req,res) {
    let pathname = url.parse(req.url).pathname;
    if(!pathname.endsWith('/')){
        pathname = pathname + '/';
    }
    if(G[pathname]){
        G[pathname](req,res);
    }else{
        res.end('no router')
    }
};

/* 定义一个 get 方法 */
app.get = function (string, callback) {
    if(!string.endsWith('/')){
        string = string + '/';
    }
    if(!string.startsWith('/')){
        string  = '/' + string;
    }
    G[string] = callback;
};

/* 只有有请求触发，就会触发app这个方法 */
http.createServer(app).listen(3000);
app.get('login',function (req,res) {
    res.end('login')
});
app.get('register', function (req,res) {
    res.end('register');
});