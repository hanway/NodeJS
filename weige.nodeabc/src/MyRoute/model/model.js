const ejs = require('ejs');

const app = {
    login(req,res){
        ejs.renderFile('views/form.ejs',{}, (err,chunk)=>{
            if(err) throw err;
            res.end(chunk)
        })
    },
    dologin(req,res){
        let strPost = '';
        req.on('data',(chunk)=>{
            strPost += chunk;
        });
        req.on('end', (err,chunk)=>{
            res.end(strPost);
        })
    },
    home(req,res){
        res.end('home');
    }
};
module.exports = app;