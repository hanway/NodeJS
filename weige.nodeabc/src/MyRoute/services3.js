/*
* GET 和 POST
* */
const http = require('http');
const url = require('url');
const ejs = require('ejs');
const queryString = require('querystring');

http.createServer((req, res) => {
    res.writeHead(200,{'Content-Type':'text/html;charset="utf8"'});
    /* 获取 get 还是 post 请求 */
    const method = req.method.toLowerCase();
    const pathname = url.parse(req.url).pathname;
    if(pathname === '/login'){
        ejs.renderFile('views/form.ejs',(err,data)=>{
            if(err) throw err;
            res.end(data);
        })
    }else if(pathname === '/dologin' && method === 'get'){
        /* 获取get数据 */
        console.log(url.parse(req.url,true).query);
        res.end('Do Login');
    }else if(pathname === '/dologin' && method === 'post'){
        let postStr = '';
        req.on('data',(chunk)=>{
            postStr += chunk;
        });
        req.on('end',(err,chunk)=>{
            res.end(JSON.stringify(queryString.parse(postStr)));
        });
    }else{
        ejs.renderFile('views/index.ejs',(err,data)=>{
            if(err) throw err;
            res.end(data);
        })
    }
}).listen(3000);