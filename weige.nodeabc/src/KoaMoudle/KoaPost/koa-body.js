/*
* Koa 中 koa-bodyparser 中间件获取表单提交数据
* 1 npm install koa-bodyparser --save
* 2 引入 const bodyParser = require('koa-bodyparser')
* 3 app.use(bodyParser())
* 4 ctx.request.body 获取数据
* */

const Koa = require('koa');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const views = require('koa-views');

const app = new Koa();
app.use(bodyParser());
app.use(views('views',{extension:'ejs'}));

router.get('/',async (ctx)=>{
    await ctx.render('index');
});

router.post('/doAdd',(ctx)=>{
    console.log(ctx.request.body);
    ctx.body = ctx.request.body;
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);