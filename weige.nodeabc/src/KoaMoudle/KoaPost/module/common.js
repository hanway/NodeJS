exports.getPostData = function(ctx){
    return new Promise((resolve, reject) => {
        try{
            let strPost = '';
            ctx.req.on('data', (chunk)=>{
                strPost += chunk
            });
            ctx.req.on('end',()=>{
                resolve(strPost);
            })
        }catch (e) {
            reject(e);
        }
    })
};