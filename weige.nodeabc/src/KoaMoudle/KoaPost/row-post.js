const Koa = require('koa');
const router = require('koa-router')();
const views = require('koa-views');

const common = require('./module/common');

const app = new Koa();
app.use(views('views',{extension:'ejs'}));

router.get('/', async (ctx)=>{
     await ctx.render('index.ejs');
});

router.post('/doAdd', async (ctx)=>{
    let data = await common.getPostData(ctx);
    console.log(data);
    ctx.body = data;
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);