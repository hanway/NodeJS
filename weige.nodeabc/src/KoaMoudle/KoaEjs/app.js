/*
* ejs 模板引擎的使用
* 1 npm install koa-views --save
* 2 npm install ejs --save
* 3 const views = require('koa-views')
*   app.use(_dirname,{extension:'ejs'}
* 4 await ctx.render('index')
*
* */

const Koa = require('koa');
const views = require('koa-views');
const router = require('koa-router')();


const app = new Koa();
/* 配置模板引擎中间件 --第三方中间件 */
/* app.use(views('views',{map:{html:'ejs'})) 这样配置也可以， 如果这样配置后缀名就是 html */
app.use(views('views',{extension:'ejs'}));

/* 写一个中间件配置公共的信息 */
app.use(async (ctx, next)=>{
    ctx.state = {
        userInfo:'张三'
    };
    await next(); // 向下继续执行
});

router.get('/', async (ctx)=>{
   let title = 'hello world';
   await ctx.render('index',{title:title})
});

router.get('/news',async (ctx)=>{
    //ctx.body='这是一个新闻';
    let list=['11111','22222','33333'];
    let content="<h2>这是一个h2</h2>";
    let num=12;
    await ctx.render('news',{
        list:list,
        content:content,
        num:num
    })
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);