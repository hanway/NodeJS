/*
*  https://www.npmjs.com/package/koa-router
*  1 安装模块
*  2 看文档使用
* */

/* 引入 Koa 模块 */
const Koa = require('koa');
const router = require('koa-router')();

const app = new Koa();

router.get('/', async (ctx)=>{
    ctx.body = '这是首页数据';
});

router.get('/news', async (ctx)=>{
    ctx.body = '这是新闻页面数据';
});

router.get('/news_content', async (ctx)=>{
    /*
    * 在 Koa 中 GET 传值通过 request 接收，但是接收方法有两种 query 和 querystring.
    * query: 返回格式化好的参数对象
    * querystring: 返回的是请求字符串
    *  */
    /* 从 ctx 里面 获取 get 的传值 */
    console.log(ctx.query);
    console.log(ctx.querystring);
    console.log(ctx.url);
    ctx.body = '12343354';
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);