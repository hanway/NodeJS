/* 引入模块 */
const Koa = require('koa');
const Router = require('koa-router');

/*实例化*/
let app = new Koa();
let router = new Router();

/* ctx 上线context 包含了 request 和 response */
router.get('/', async (ctx)=>{
    /* 类似原声里面的 res.writeHeader res.end */
   ctx.body = '首页';
}).get('/news', async (ctx)=>{
    ctx.body = '这是一个新闻页面';
});

/* 启动路由 */
app
    .use(router.routes()) /*启动路由*/
    .use(router.allowedMethods());
/*
 router.allowedMethods()作用： 这是官方文档的推荐用法,我们可以
 看到 router.allowedMethods()用在了路由匹配 router.routes()之后,所以在当所有
 路由中间件最后调用.此时根据 ctx.status 设置 response 响应头
*/

app.listen(3000);