/*
* https://www.npmjs.com/package/koa-router
* 1 安装模块
* 2 按照文档使用
* */

/* 引入 Koa 模块 */
const Koa = require('koa');
const router = require('koa-router')();

/* 实例化 */
const app = new Koa();

router.get('/', async (ctx)=>{
    ctx.body = '这是首页数据';
});

router.get('/news', async (ctx)=>{
    ctx.body = '这是新闻页面数据';
});

router.get('/news_content', async (ctx)=>{
    ctx.body = ctx.querystring
});

/* 动态路由 */
router.get('/news_content/:aid', async (ctx)=>{
    ctx.body = JSON.stringify(ctx.params);
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);