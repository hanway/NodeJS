const path = require('path');
const Koa = require('koa');
const router = require('koa-router')();
const render = require('koa-art-template');

/* 路由模块 start */
const index = require('./routes/index');
const api = require('./routes/api');
const admin = require('./routes/admin');
/* 路由模块 end */

const app = new Koa();
render(app,{
    root: path.resolve(__dirname, 'views'),
    extname: '.html',
    debug: process.env.NODE_ENV !== 'production'
});


router.use('/', index);
router.use('/api', api);
router.use('/admin', admin);

/* 最后处理无匹配页面 */
router.get('*', async (ctx)=>{
    ctx.body = '<h2>404</h2>'
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);