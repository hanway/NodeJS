const router = require('koa-router')();

const user = require('./admin/user');
const focus = require('./admin/focus');
const newscate = require('./admin/newscate');

router.use('/user', user);
router.use('/newscate', newscate);
router.use('/focus', focus);

module.exports = router.routes();