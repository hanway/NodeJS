const router = require('koa-router')();

router.get('/', async (ctx)=>{
    await ctx.render('default/index');
});

router.get('case', async (ctx)=>{
    ctx.body = '<h2>案例</h2>';
});

router.get('about', async (ctx)=>{
    await ctx.render('default/about');
});

module.exports = router.routes();