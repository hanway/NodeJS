const router = require('koa-router')();

router.get('/', async (ctx)=>{
    await ctx.render('admin/user/index')
});

router.get('/add', async (ctx)=>{
    await ctx.render('admin/user/add')
});

router.get('/edit', async (ctx)=>{
    await ctx.render('admin/user/edit')
});

router.get('/delete', async (ctx)=>{
    ctx.body = '删除这是删除页面'
});

module.exports = router.routes();