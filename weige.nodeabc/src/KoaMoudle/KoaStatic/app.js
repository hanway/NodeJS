/*
* Koa 使用静态资源
* 1 npm install koa-static --save
* 2 引入 const static = require('koa-static');
* 3 配置中间件 app.use(static('static'));
* */

const Koa = require('koa');
const router = require('koa-router')();
const views = require('koa-views');
const bodyParse = require('koa-bodyparser');
const statics = require('koa-static');

const app = new Koa();
app.use(bodyParse());
app.use(statics(__dirname+'/static'));
app.use(statics(__dirname+'/public'));
app.use(views('views',{map:{'html':'ejs'}}));

router.get('/', async (ctx)=>{
    await ctx.render('index');
});

router.post('/doAdd', async (ctx)=>{
    ctx.body = ctx.request.body;
});

app.use(router.routes());
app.listen(3000);