/*
    1.cookie保存在浏览器客户端

    2.可以让我们用同一个浏览器访问同一个域 名的时候共享数据

/*
    1、保存用户信息
    2、浏览器历史记录
    3、猜你喜欢的功能
    4、10天免登陆
    5、多个页面之间的数据传递
    6、cookie实现购物车功能
*/
const path = require('path');
const Koa = require('koa');
const router = require('koa-router')();
const render = require('koa-art-template');

const app = new Koa();
render(app,{
    root:path.resolve(__dirname,'views'),
    extname:'.html',
    debug: process.env.NODE_ENV !== 'production'
});

router.get('/', async (ctx)=>{
    /*koa 中没法直接设置中文的cookie*/
    const userinfo = new Buffer('张三').toString('base64');
    ctx.cookies.set('userinfo',userinfo,{
        maxAge:60*1000*60
    });
    let list={
        name:'张三'
    };
    await ctx.render('index',{
        list:list

    });
});

router.get('/news',async (ctx)=>{
    const data=ctx.cookies.get('userinfo');
    const userinfo=new Buffer(data, 'base64').toString();
    console.log(userinfo);

    let app={
        name:'张三11'
    };
    await ctx.render('news',{
        list:app
    });
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);