/*
* 1 cookie 保存在浏览器客户端
* 2 可以让我们同一个浏览器访问同一个域名的时候共享数据
*
*
* 1 保存用户信息
* 2 浏览器历史记录
* 3 猜你喜欢
* 4 10天免登录
* 5 多个页面之间的数据传递
* 6 cookie实现购物车功能
*
* ctx.cookies.set('userinfo','zhangsan', { maxAge: 60*10000*60 })
*
* const userinfo = ctx.cookies.get('userinfo')
*
* */
const path = require('path');
const Koa = require('koa');
const router = require('koa-router')();
const render = require('koa-art-template');

const app = new Koa();
render(app,{
    root: path.resolve(__dirname,'views'),
    extname:'.html',
    debug:process.env.NODE_ENV !== 'production'
});

router.get('/', async (ctx)=>{
    /*
    * 正常配置
    * crx.cookies.set('userinfo','张三',{
    *   maxAge: 60*1000*60
    * })
    *
    * */

    ctx.cookies.set('userinfo','张三',{
        maxAge: 60*1000*60,
        // path: '/news', /*配置可以访问的页面*/
        // domain:'.sinochem.com', /*正常情况下不要设置 默认就是当前域下面的所有页面都可以访问*/
        httpOnly:false, /*true 表示这个cookie只有服务端可以访问，false表示客户端（js）和 服务器端都可以访问*/
    });
    const list = {
        name: '张三'
    };

    await ctx.render('index',{list});
});

router.get('/about', async (ctx)=>{
    ctx.cookies.set('userinfo','zhangsan33333',{
        maxAge:60*1000*60,
        path:'/news'
    });

    ctx.body="这是关于我们";
});

router.get('/news',async (ctx)=>{

    let userinfo=ctx.cookies.get('userinfo');

    console.log(userinfo);
    let app={
        name:'张三11'
    };
    await ctx.render('news',{
        list:app
    });
});

router.get('/shop',async (ctx)=>{

    let userinfo=ctx.cookies.get('userinfo');

    console.log(userinfo);
    ctx.body='这是一个商品页面'+userinfo;
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);