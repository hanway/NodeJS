/*
* 1 npm install koa-session -save
*
* 2 const session = require('koa-session')
*
* 3
*  app.keys = ['some secret hurr'];

 const CONFIG = {
 key: 'koa:sess',
maxAge: 86400000,
    overwrite: true,
    httpOnly: true,
    signed: true,
    rolling: false,
    renew: false,
};

app.use(session(CONFIG, app));


设置 session
ctx.session.username = "张三"

获取 session
 ctx.session.username
*
* */
const path = require('path');
const Koa = require('koa');
const router = require('koa-router')();
const render = require('koa-art-template');
const sessions = require('koa-session');

const app = new Koa();
render(app,{
    root: path.resolve(__dirname,'views'),
    extname: '.html',
    debug: process.env.NODE_ENV !== 'production'
});
/* 配置 session 中间件 */
app.keys = ['some secret hurr'];   /*cookie的签名*/
const CONFIG = {
    key: 'koa:sess', /** 默认 */
    maxAge: 10000,  /*  cookie的过期时间        【需要修改】  */
    overwrite: true, /** (boolean) can overwrite or not (default true)    没有效果，默认 */
    httpOnly: true, /**  true表示只有服务器端可以获取cookie */
    signed: true, /** 默认 签名 */
    rolling: true, /** 在每次请求时强行设置 cookie，这将重置 cookie 过期时间（默认：false） 【需要修改】 */
    renew: false, /** (boolean) renew session when session is nearly expired      【需要修改】*/
};
app.use(sessions(CONFIG,app));

router.get('/', async (ctx)=>{
    /* 获取 session */
    console.log(ctx.session.userinfo);
    ctx.body = '登录成功';
});

router.get('/login', async (ctx)=>{
    ctx.session.userinfo = 'zhangsan';
    ctx.body = '登录成功'
});

router.get('/news',async (ctx)=>{

    //获取session
    console.log(ctx.session.userinfo);
    ctx.body="登录成功";
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);