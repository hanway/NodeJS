const http = require('http');
/*
* 引入http模块
* 创建服务器，参数是一个回调函数，表示如果有请求进来，要做什么
* req: request 表示请求
* res: response 表示响应
* */
http.createServer(((req, res) => {
    /*
    * 设置响应头
    * 状态码是 200，文件类型是 html，字符集是 utf-8
    * */
    res.writeHead(200,{"Content-Type":"text/html;charset='utf-8'"});
    /* 解决页面乱码问题 */
    res.write('<head><title>Node</title><meta charset="utf-8"></head>');
    res.write('你好 Node.js');
    res.write('<h2>你好 nodejs</h2>');
    /* 必须有这句话，结束响应，防止出现假死现象 */
    res.end();
})).listen(3000);