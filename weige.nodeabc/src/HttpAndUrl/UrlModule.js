const http = require('http');
const url = require('url');
const queryString = require('querystring');
const utils = require('../module/utils');
const request = require('../module/request');
http.createServer(((req, res) => {
    res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'});
    res.write('<head><meta charset="UTF-8"><title>Url Module</title></head>');
    if(req.url !== '/favicon.ico'){
        const userInfo = url.parse(req.url,true).query;
        res.write(`<code>${JSON.stringify(userInfo)}</code>`);
        if(req.url.indexOf('?')){
            let urlString = req.url.slice(req.url.indexOf('?')+1);
            const info = queryString.parse(urlString);
            res.write(`<br><code>${JSON.stringify(info)}</code>`);
        }
        let strFormatUrl = utils.formatUrl(req.url);
        res.write(`<h3>${strFormatUrl}</h3>`);
        request.printInfo();
    }
    res.end();
})).listen(3000);