/*
* nodejs event 模块
* */
const event = require('events');
const fs = require('fs');
const eventEmitter = new event.EventEmitter();

/*
* 广播和接受广播
* */
/*
eventEmitter.on('to_parent',(data)=>{
    console.log('接收到了这个广播');
    console.log(data);
});

setTimeout(()=>{
    eventEmitter.emit('to_parent', '发送的数据')
},2000);*/


/*
* 获取异步的两种方法
* */
/* 1 回调函数 */
/*getText = function(callBack){
    fs.readFile('./text.txt',(err, data)=>{
        if(err) throw err;
        callBack(data);
    })
};
getText((data)=>{console.log(data.toString())});*/

/* 2 使用广播和接受广播 */
/*
eventEmitter.on('read_file',(data)=>{
    console.log(data.toString());
});
fs.readFile('./text.txt', (err, data)=>{
    if(err) throw err;
    eventEmitter.emit('read_file',data);
});*/
