/*
* NodeJs 没有 Web 容器
* */
const http = require('http');
const fs = require('fs');
http.createServer(((req, res) => {
    console.log(req.url);
    if(req.url === '/index'){
        fs.readFile('./index.html',(err,data) => {
            if(err) throw err;
            res.writeHead(200,{'Content-Type':'text/html;charset=UTF8'});
            res.end(data);
        })
    }else if (req.url === '/css.css'){
        fs.readFile('./css.css',(err, data) => {
            if(err) throw err;
            res.writeHead(200,{'Content-Type':'text/css;charset=UFT-8'});
            res.end(data);
        })
    }else if (req.url === '/mouse.jpg'){
        fs.readFile('./mouse.jpg',(err, data) => {
            if(err) throw err;
            res.writeHead(200, {'Content-Type':'image/jpg'});
            res.end(data);
        })
    }else{
        res.writeHead(200,{'Content-Type':'text/html;charset=UTF-8'});
        res.end("嘻嘻，您访问的页面不存在哟！")
    }
})).listen(3000);