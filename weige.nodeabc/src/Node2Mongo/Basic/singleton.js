/*  ES5中的继承
*
* 原型链继承和对象冒充继承
* 对象冒充继承：没法继承原型链上的属性和方法
* 原型链继承：可以继承构造函数里面以及原型链上面的属性和方法，实例化子类的时候没法给父类传参
*
* */
/*function Person(name,age){
    this.name = name;
    this.age = age;
    this.run = function () {
        console.log(`${this.name}---${this.age}`);
    }
}
Person.prototype.work = function () {
    console.log('work')
};
function Worker(name,age) {
    Person.call(this,name,age);
}
Worker.prototype = new Person();
let w = new Worker('zhangsan',18);

w.run();
w.work();*/





/* ES6 中单里模式 */
class Db{
    static getInstance(){
        if(!Db.instance){
            Db.instance = new Db();
        }
        return Db.instance;
    }
    constructor(){
       console.log('实例化会触发构造函数');
       this.connect();
    }
    connect(){
        console.log('连接数据库');
    }
    find(){
        console.log('查询数据库');
    }
}

const myDb1 = Db.getInstance();
const myDb2 = Db.getInstance();
const myDb3 = Db.getInstance();

myDb2.find();
myDb3.find();
